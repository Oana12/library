package com.company;

public abstract class Library {
    private String title;
    private String author;
    private Genre gen;

    public Library(String title, String author, Genre gen) {
        this.title = title;
        this.author = author;
        this.gen= gen;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Genre getGen() {
        return gen;
    }

    public void setGen(Genre gen) {
        this.gen = gen;
    }

}

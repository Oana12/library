package com.company;

public abstract class Books extends Library {
    private String ISBN;


    public Books(String ISBN,String title, String author, Genre gen) {
        super(title, author, gen);
        this.ISBN = ISBN;
    }

}

package com.company;

/*
    1. Modeling (Class, Abstract Class, Interface, Enum, Exception, Collections, ArrayList, Encapsulation, Abstraction, Composition, Polymorphism)
    Create a program that will help manage a library. So far we will store
    - books (they have title, author(s), genre (eg like Fantasy, Self-Help, Science), and a unique identifier: ISBN (International Standard Book Number))
    - scientific articles (they also have title, author(s), a theme (eg: Computer Science, Biology, Psychology) and a unique identifier DOI)
    - movies ( they have title and genre, a list of participating actors, and they can be identified via an IMDB id)

    2. Read from file
    We would need to read books, articles and movies from a file and store them.

    3. Search through data
    We would like to be able to query
        - books by their title
        - by one of the authors
 */

import java.io.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader("reader.txt"));
        BufferedWriter writer = new BufferedWriter(new FileWriter("writer.txt"));

        ArrayList<Library> publications = new ArrayList<>();
        String line;
        int count = 0;
        while ((line = reader.readLine()) != null) {
            String[] splitLine = line.split("-");
            if ( splitLine[0].equals("B")) {
                String title = splitLine[1];

            }
        }



        reader.close();
        writer.close();

    }
}

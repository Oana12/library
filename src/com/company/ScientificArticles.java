package com.company;

public class ScientificArticles extends Library {
    private String DOI;


    public ScientificArticles(String title, String author, String DOI, Genre gen) {
        super(title, author, gen);
        this.DOI = DOI;
    }
}
